<?php
namespace SatSuite\Support\Contracts;

interface NamespacesContract
{
    public function has($node);

    public function get($node);

    public function all();

    public function used();

    public function reset();
}
