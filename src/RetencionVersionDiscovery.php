<?php
namespace SatSuite\Support;

use UnexpectedValueException;
use SatSuite\Support\Traits\WithXmlLoader;

class RetencionVersionDiscovery
{
    use WithXmlLoader;

    public function make($xml)
    {
        $dom = $this->getXmlAsDomDocument($xml);

        $element = $dom->documentElement;

        $version = $element->getAttribute('Version');

        unset($sxml, $file);

        if (!$version) {
            throw new UnexpectedValueException('El xml proporcionado no contiene la versión, no es un CFDI de retenciones.');
        }

        return $version;
    }

}