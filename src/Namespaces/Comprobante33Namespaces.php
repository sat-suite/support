<?php
namespace SatSuite\Support\Namespaces;

class Comprobante33Namespaces extends AbstractNamespaces
{
    protected $namespaces = [
        'Comprobante' => [
            'cfdi',
            'http://www.sat.gob.mx/cfd/3 ',
            'http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd'
        ],
        'Pagos' => [
            'pago10',
            'http://www.sat.gob.mx/Pagos',
            'http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd'
        ],
        'ComercioExterior' => [
            'cce11',
            'http://www.sat.gob.mx/ComercioExterior11',
            'http://www.sat.gob.mx/sitio_internet/cfd/ComercioExterior11/ComercioExterior11.xsd'
        ],
        'Nomina' => [
            'nomina12',
            'http://www.sat.gob.mx/nomina12',
            'http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd'
        ],
        'TimbreFiscalDigital' => [
            'tfd',
            'http://www.sat.gob.mx/TimbreFiscalDigital',
            'http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd'
        ],
        'Divisas' => [
            'divisas',
            'http://www.sat.gob.mx/divisas',
            'http://www.sat.gob.mx/sitio_internet/cfd/divisas/divisas.xsd'
        ],
        'acreditamientoIEPS' => [
            'aieps',
            'http://www.sat.gob.mx/acreditamiento',
            'http://www.sat.gob.mx/sitio_internet/cfd/acreditamiento/AcreditamientoIEPS10.xsd'
        ],
        'Aerolineas' => [
            'aerolineas',
            'http://www.sat.gob.mx/aerolineas',
            'http://www.sat.gob.mx/sitio_internet/cfd/aerolineas/aerolineas.xsd'
        ],
        'certificadodedestruccion' => [
            'destruccion',
            'http://www.sat.gob.mx/certificadodestruccion',
            'http://www.sat.gob.mx/sitio_internet/cfd/certificadodestruccion/certificadodedestruccion.xsd'
        ],
        'CFDIRegistroFiscal' => [
            'registrofiscal',
            'http://www.sat.gob.mx/registrofiscal',
            'http://www.sat.gob.mx/sitio_internet/cfd/cfdiregistrofiscal/cfdiregistrofiscal.xsd'
        ],
        'ConsumoDeCombustibles' => [
            'consumodecombustibles11',
            'http://www.sat.gob.mx/consumodecombustibles',
            'http://www.sat.gob.mx/sitio_internet/cfd/ConsumoDeCombustibles/consumodeCombustibles11.xsd'
        ],
        'detallista' => [
            'detallista',
            'http://www.sat.gob.mx/detallista',
            'http://www.sat.gob.mx/sitio_internet/cfd/detallista/detallista.xsd'
        ],
        'Donatarias' => [
            'donat',
            'http://www.sat.gob.mx/donat',
            'http://www.sat.gob.mx/sitio_internet/cfd/donat/donat11.xsd'
        ],
        'EstadoDeCuentaCombustible' => [
            'ecc11',
            'http://www.sat.gob.mx/EstadoDeCuentaCombustible',
            'http://www.sat.gob.mx/sitio_internet/cfd/EstadoDeCuentaCombustible/ecc12.xsd'
        ],
        'instEducativas' => [
            'iedu',
            'http://www.sat.gob.mx/iedu',
            'http://www.sat.gob.mx/sitio_internet/cfd/iedu/iedu.xsd'
        ],
        'ImpuestosLocales' => [
            'implocal',
            'http://www.sat.gob.mx/implocal',
            'http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd'
        ],
        'INE' => [
            'ine',
            'http://www.sat.gob.mx/ine',
            'http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd'
        ],
        'LeyendasFiscales' => [
            'leyendasFisc',
            'http://www.sat.gob.mx/leyendasFiscales',
            'http://www.sat.gob.mx/sitio_internet/cfd/leyendasFiscales/leyendasFisc.xsd'
        ],
        'NotariosPublicos' => [
            'notariospublicos',
            'http://www.sat.gob.mx/notariospublicos',
            'http://www.sat.gob.mx/sitio_internet/cfd/notariospublicos/notariospublicos.xsd'
        ],
        'obrasarteantiguedades' => [
            'obrasarte',
            'http://www.sat.gob.mx/arteantiguedades',
            'http://www.sat.gob.mx/sitio_internet/cfd/arteantiguedades/obrasarteantiguedades.xsd'
        ],
        'PagoEnEspecie' => [
            'pagoenespecie',
            'http://www.sat.gob.mx/pagoenespecie',
            'http://www.sat.gob.mx/sitio_internet/cfd/pagoenespecie/pagoenespecie.xsd'
        ],
        'PFintegranteCoordinado' => [
            'pfic',
            'http://www.sat.gob.mx/pfic',
            'http://www.sat.gob.mx/sitio_internet/cfd/pfic/pfic.xsd'
        ],
        'renovacionysustitucionvehiculos' => [
            'decreto',
            'http://www.sat.gob.mx/renovacionysustitucionvehiculos',
            'http://www.sat.gob.mx/sitio_internet/cfd/renovacionysustitucionvehiculos/renovacionysustitucionvehiculos.xsd'
        ],
        'parcialesconstruccion' => [
            'servicioparcial',
            'http://www.sat.gob.mx/servicioparcialconstruccion',
            'http://www.sat.gob.mx/sitio_internet/cfd/servicioparcialconstruccion/servicioparcialconstruccion.xsd'
        ],
        'PorCuentadeTerceros' => [
            'terceros',
            'http://www.sat.gob.mx/terceros',
            'http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd'
        ],
        'TuristaPasajeroExtranjero' => [
            'tpe',
            'http://www.sat.gob.mx/TuristaPasajeroExtranjero',
            'http://www.sat.gob.mx/sitio_internet/cfd/TuristaPasajeroExtranjero/TuristaPasajeroExtranjero.xsd'
        ],
        'ValesDeDespensa' => [
            'valesdedespensa',
            'http://www.sat.gob.mx/valesdedespensa',
            'http://www.sat.gob.mx/sitio_internet/cfd/valesdedespensa/valesdedespensa.xsd'
        ],
        'VehiculoUsado' => [
            'vehiculousado',
            'http://www.sat.gob.mx/vehiculousado',
            'http://www.sat.gob.mx/sitio_internet/cfd/vehiculousado/vehiculousado.xsd'
        ],
        'VentaVehiculos' => [
            'ventavehiculos',
            'http://www.sat.gob.mx/ventavehiculos',
            'http://www.sat.gob.mx/sitio_internet/cfd/ventavehiculos/ventavehiculos11.xsd'
        ],
        'CartaPorte' => [
            'cartaporte20',
            'http://www.sat.gob.mx/CartaPorte20',
            'http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte20.xsd'
        ]
    ];
}
