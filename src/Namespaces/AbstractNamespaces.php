<?php
namespace SatSuite\Support\Namespaces;

use SatSuite\Support\Contracts\NamespacesContract;

abstract class AbstractNamespaces implements NamespacesContract
{
    protected $namespaces = [];
    protected $usedNamespaces = [];

    public function has($namespace)
    {
        return isset($this->namespaces[$namespace]);
    }

    public function get($namespace, $default = null)
    {
        if ($this->has($namespace)) {
            $value = $this->namespaces[$namespace];

            $this->usedNamespaces[$namespace] = $value;

            return $value;
        }

        return $default;
    }

    public function all()
    {
        return $this->namespaces;
    }

    public function used()
    {
        return $this->usedNamespaces;
    }

    public function reset()
    {
        return $this->usedNamespaces = [];
    }
}
