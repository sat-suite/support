<?php
namespace SatSuite\Support\Namespaces;

class Retenciones20Namespaces extends AbstractNamespaces
{
    protected $namespaces = [
        'Retenciones' => [
            'retenciones',
            'http://www.sat.gob.mx/esquemas/retencionpago/2',
            'http://www.sat.gob.mx/esquemas/retencionpago/2/retencionpagov2.xsd'
        ],
        'Arrendamientoenfideicomiso' => [
            'arrendamientoenfideicomiso',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/arrendamientoenfideicomiso',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/arrendamientoenfideicomiso/arrendamientoenfideicomiso.xsd'
        ],
        'Dividendos' => [
            'dividendos',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos/dividendos.xsd'
        ],
        'EnajenaciondeAcciones' => [
            'enajenaciondeacciones',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones/enajenaciondeacciones.xsd',
        ],
        'Fideicomisonoempresarial' => [
            'fideicomisonoempresarial',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/fideicomisonoempresarial',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/fideicomisonoempresarial/fideicomisonoempresarial.xsd',
        ],
        'Intereses' => [
            'intereses',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/intereses',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/intereses/intereses.xsd',
        ],
        'Intereseshipotecarios' => [
            'intereseshipotecarios',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/intereseshipotecarios',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/intereseshipotecarios/intereseshipotecarios.xsd',
        ],
        'Operacionesconderivados' => [
            'operacionesconderivados',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados/operacionesconderivados.xsd',
        ],
        'Pagosaextranjeros' => [
            'pagosaextranjeros',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros/pagosaextranjeros.xsd',
        ],
        'Planesderetiro' => [
            'planesderetiro11',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/planesderetiro11',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/planesderetiro11/planesderetiro11.xsd',
        ],
        'Premios' => [
            'premios',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/premios',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/premios/premios.xsd',
        ],
        'SectorFinanciero' => [
            'sectorfinanciero',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero/sectorfinanciero.xsd',
        ],
        'ServiciosPlataformasTecnologicas' => [
            'plataformasTecnologicas',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/PlataformasTecnologicas10',
            'http://www.sat.gob.mx/esquemas/retencionpago/1/PlataformasTecnologicas10/ServiciosPlataformasTecnologicas10.xsd'
        ],
        'TimbreFiscalDigital' => [
            'tfd',
            'http://www.sat.gob.mx/TimbreFiscalDigital',
            'http://www.sat.gob.mx/TimbreFiscalDigital/TimbreFiscalDigital.xsd',
        ],
    ];
}
