<?php

if (!is_callable('random_bytes'))
{
    /**
     *
     * @param int $bytes
     *
     * @throws Exception
     *
     * @return string
     */
    function random_bytes($length)
    {
        if ($length < 1) {
            throw new Error('Length must be greater than 0');
        }

        if (function_exists('mcrypt_create_iv')) {
            $key = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
        }

        if (function_exists('openssl_random_pseudo_bytes')) {
            $strong = true;
            $key = openssl_random_pseudo_bytes($length, $strong);

            if ($strong === false) {
                throw new Exception('openssl_random_pseudo_bytes() set $crypto_strong false. Your PHP setup is insecure.');
            }
        }

        if ($key !== false && mb_strlen($key, '8bit') === $length) {
            return $key;
        }

        throw new Exception('Unable to generate a random key.');
    }
}

/**
 * https://www.php.net/manual/es/function.random-int.php#119670
 */
if (!is_callable('random_int'))
{
    function random_int($min = 0, $max = 0xffff) {
        $min = (int)$min;
        $max = (int)$max;

        if ($min > $max) {
            throw new Exception('$max can\'t be lesser than $min');
        }

        if ($max === $min) {
            return (int) $min;
        }

        $range = $counter = $max - $min;
        $bits = 1;

        while ($counter >>= 1) {
            ++$bits;
        }

        $bytes = (int)max(ceil($bits/8), 1);
        $bitmask = pow(2, $bits) - 1;

        if ($bitmask >= PHP_INT_MAX) {
            $bitmask = PHP_INT_MAX;
        }

        do {
            $result = hexdec(bin2hex(random_bytes($bytes))) & $bitmask;
        } while ($result > $range);

        return $result + $min;
    }
}