<?php
namespace SatSuite\Support\Traits;

use DOMDocument;
use SimpleXMLElement;

trait WithXmlLoader
{
    protected $config = [];

    public function getXmlAsDomDocument($xml)
    {
        if ($xml instanceof DOMDocument) {
            return $xml;
        }

        $content = $this->getXmlAsString($xml);

        $dom = new DOMDocument($content);
        $dom->loadXML($content);

        unset($xml, $content);

        return $dom;
    }

    public function getXmlAsSimpleXmlElement($xml)
    {
        if ($xml instanceof SimpleXMLElement) {
            return $xml;
        }

        $content = $this->getXmlAsString($xml);

        $sxml = new SimpleXMLElement($content);

        unset($xml, $content);

        return $sxml;
    }

    public function getXmlAsString($xml)
    {
        $content = null;

        if ($xml instanceof SimpleXMLElement) {
            $content = $xml->asXML();
        } else if ($xml instanceof DOMDocument) {
            $content = $xml->saveXML();
        } else {
            $file = strval(str_replace("\0", '', $xml));

            if (file_exists($file)) {
                $content = file_get_contents($file);
            } else {
                $content = $xml;
            }

            unset($file);
        }

        unset($xml);

        return $content;
    }

}
