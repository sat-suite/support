<?php
namespace SatSuite\Support\Traits;

trait WithTempFileStorageManager
{
    protected $tmp;
    protected $config = [];

    public function getTmpPath($root = null)
    {
        $path = $this->tmp;

        if (!$path) {
            if (isset($this->config['storage'])) {
                $path = rtrim($this->config['storage'], DIRECTORY_SEPARATOR);
            } else {
                $path = sys_get_temp_dir();
            }

            $path =  $path . DIRECTORY_SEPARATOR . $root . DIRECTORY_SEPARATOR;
        }

        if (!file_exists($path)) {
            mkdir($path, 0775, true);
        }

        return $this->tmp = $path;
    }

    public function getTmpPathForFile($name, $path = 'tmp')
    {
        return $this->getTmpPath($path) . $name;
    }

}