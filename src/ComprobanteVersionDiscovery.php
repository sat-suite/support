<?php
namespace SatSuite\Support;

use UnexpectedValueException;
use SatSuite\Support\Traits\WithXmlLoader;

class ComprobanteVersionDiscovery
{
    use WithXmlLoader;

    public function make($xml)
    {
        $dom = $this->getXmlAsDomDocument($xml);

        $element = $dom->documentElement;

        $version = '4.0';

        if ($element->getAttribute('version') === '3.2') {
            $version = '3.2';
        } else {
            $version = $element->getAttribute('Version');
        }

        unset($dom, $xml, $element);

        if (!$version) {
            throw new UnexpectedValueException('El xml proporcionado no contiene la versión, no es un CFDI.');
        }

        return $version;
    }

}