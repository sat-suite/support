<?php
namespace SatSuite\Support;

use SatSuite\Support\Exceptions\ShellCommandException;

class Shell
{
    protected $stdin;

    protected $display;

    protected $throwExceptions = true;

    protected $error;

    protected $output;

    /**
     * Execute a command from linux shell
     *
     * @param string $command
     *
     * @return mixed
     */
    public function execute($command)
    {
        $result = $this->run($command);

        return $result === 0 ? ($this->output ? : true) : false;
    }

    public function display(callable $display)
    {
        $this->display = $display;

        return $this;
    }

    public function exceptions($exceptions)
    {
        $this->throwExceptions = (boolval($exceptions));

        return $this;
    }

    public function getError()
    {
        return $this->error;
    }

    public function getOutput()
    {
        return $this->output;
    }

    protected function run($command)
    {
        $specs = array(
            0 => array('pipe', 'r'), // Stdout
            1 => array('pipe', 'w'), // Stdin
            2 => array('pipe', 'w') // Stderr
        );

        $process = proc_open($command, $specs, $pipes);

        if (is_resource($process)) {
            fwrite($pipes[0], $this->stdin);
            fclose($pipes[0]);

            $output = '';

            $callback = $this->display;

            while(!feof($pipes[1])) {
                $stdout = fgets($pipes[1], 1024);

                if (strlen($stdout) == 0) {
                    break;
                }

                !!$callback && $callback($stdout);

                $output .= $stdout;
            }

            $error = trim(stream_get_contents($pipes[2]));

            !!$callback && $callback($error, true);

            $this->output = $output;
            $this->error = $error;

            fclose($pipes[1]);
            fclose($pipes[2]);

            $result = proc_close($process);

            if ($this->throwExceptions && $result != 0) {
                throw new ShellCommandException($error, $result);
            }

            return $result;
        }
    }

}
